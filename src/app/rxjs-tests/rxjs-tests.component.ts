import { Component, OnInit } from '@angular/core';
import { from, fromEvent, interval, Observable, of, range } from 'rxjs';
import { debounceTime, delay, filter, map, sampleTime, startWith, take } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs-tests',
  templateUrl: './rxjs-tests.component.html',
  styleUrls: ['./rxjs-tests.component.css']
})
export class RxjsTestsComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    // ---
    // single value
    // const stream$ = Observable.create(observer => observer.next(1));
    const stream$ = of(1, 2, 3, 4);

    // array
    // const stream$ = from([1, 2, 3, 4]);

    stream$.subscribe(data => console.log('stream$ data:', data));

    // ---
    const stream2$ = Observable.create(observer => {
      const producer = new Producer();

      while (producer.hasValues()) {
        observer.next(producer.next());
      }
    });

    stream2$.subscribe(data => console.log('stream2$ data:', data));

    // ---
    const streamWithError$ = Observable.create(observer => {
      observer.error('We have an error');
    });

    streamWithError$.subscribe(
      data => console.log('streamWithError$ data:', data),
      error => console.log('streamWithError$ err:', error)
    );

    // ---
    const streamWithCompletion$ = Observable.create(observer => {
      observer.next(1);
      observer.complete();
    });

    streamWithCompletion$.subscribe(
      data => console.log('streamWithCompletion$ data:', data),
      error => console.log('streamWithCompletion$ err:', error),
      () => console.log('streamWithCompletion$ completed')
    );

    // ---
    // const streamWithOperators$ = from([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]).pipe(
    const streamWithOperators$ = range(0, 20).pipe(
      map(x => x + 100),
      filter(x => x % 2 === 0)
    );

    streamWithOperators$.subscribe(data => console.log('streamWithOperators$ data:', data));

    // ---
    const intervalStream$ = interval(2000).pipe(
      startWith(77),
      take(5)
    );

    // ---
    intervalStream$.subscribe(data => console.log('intervalStream$ data:', data));

    const mousemove$ = fromEvent(document, 'mousemove').pipe(
      // delay(1000)
      sampleTime(200),
      // debounceTime(100)
    );
    mousemove$.subscribe((data: MouseEvent) => console.log('mouse move', data.x, data.y));
  }

}

class Producer {
  counterMax = 5;
  current = 0;

  hasValues() {
    return this.current < this.counterMax;
  }

  next() {
    return this.current++;
  }
}
