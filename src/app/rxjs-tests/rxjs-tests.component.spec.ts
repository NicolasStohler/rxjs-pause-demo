import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RxjsTestsComponent } from './rxjs-tests.component';

describe('RxjsTestsComponent', () => {
  let component: RxjsTestsComponent;
  let fixture: ComponentFixture<RxjsTestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RxjsTestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RxjsTestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
