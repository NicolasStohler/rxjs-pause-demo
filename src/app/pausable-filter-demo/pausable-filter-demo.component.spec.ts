import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PausableFilterDemoComponent } from './pausable-filter-demo.component';

describe('PausableFilterDemoComponent', () => {
  let component: PausableFilterDemoComponent;
  let fixture: ComponentFixture<PausableFilterDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PausableFilterDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PausableFilterDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
