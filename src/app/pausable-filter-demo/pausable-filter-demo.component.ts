import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, EMPTY, iif, Observable, timer } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-pausable-filter-demo',
  templateUrl: './pausable-filter-demo.component.html',
  styleUrls: ['./pausable-filter-demo.component.css']
})
export class PausableFilterDemoComponent implements OnInit {

  pauser$ = new BehaviorSubject<boolean>(true);
  timedProducer$: Observable<string>;
  itemLoader$: Observable<string>;

  constructor() { }

  ngOnInit() {

    // tick every second
    const timer$ = timer(0, 1000);

    // produce items from timer ticks
    this.timedProducer$ = timer$.pipe(
      map(t => `item #${t}`),
      // tap(t => console.log('produce item', t))
    );

    // pause using a filter (pauser$)
    // NOTE: this will keep the timer$/timedProducer$ active, so they
    // will keep producing more items in the background
    this.itemLoader$ = this.timedProducer$.pipe(
      filter(() => !this.pauser$.getValue()),
      tap(t => console.log('display item', t))
    );
  }

  togglePause() {
    const switchToPaused = !this.pauser$.getValue();
    if (switchToPaused) {
      console.log('pausing now');
    } else {
      console.log('resume now');
    }

    this.pauser$.next(switchToPaused);
  }

}
