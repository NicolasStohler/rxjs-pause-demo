import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PausableDemoComponent } from './pausable-demo.component';

describe('PausableDemoComponent', () => {
  let component: PausableDemoComponent;
  let fixture: ComponentFixture<PausableDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PausableDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PausableDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
