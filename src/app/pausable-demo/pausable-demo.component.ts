import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, EMPTY, iif, Observable, of, Subject, timer } from 'rxjs';
import { concatMap, map, scan, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-pausable-demo',
  templateUrl: './pausable-demo.component.html',
  styleUrls: ['./pausable-demo.component.css']
})
export class PausableDemoComponent implements OnInit {

  pauser$ = new BehaviorSubject<boolean>(true);
  itemLoader$: Observable<string>;

  constructor() { }

  ngOnInit() {

    // tick every second
    const timer$ = timer(0, 1000);

    // produce items from timer ticks
    const timedProducer$ = timer$.pipe(
      map(t => `item #${t}`),
      // tap(t => console.log('produce item', t))
    );

    // https://stackoverflow.com/a/41082143/54159
    // - Observable.if() is now "iif"
    // - Observable.empty is now EMPTY
    this.itemLoader$ = this.pauser$.pipe(
      // Close old streams (also called flatMapLatest)
      switchMap((active: boolean) => {
        // If the stream is active return the source
        // Otherwise return an empty Observable.
        return iif(() => active, EMPTY, timedProducer$);
      }),
      tap(t => console.log('item loader', t))
    );
  }

  togglePause() {
    const switchToPaused = !this.pauser$.getValue();
    if (switchToPaused) {
      console.log('pausing now');
    } else {
      console.log('resume now');
    }

    this.pauser$.next(switchToPaused);
  }
}
