import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PausableDemoComponent } from './pausable-demo/pausable-demo.component';
import { PausableFilterDemoComponent } from './pausable-filter-demo/pausable-filter-demo.component';
import { PausableFilterWithFilterDemoComponent } from './pausable-filter-with-filter-demo/pausable-filter-with-filter-demo.component';
import { RxjsTestsComponent } from './rxjs-tests/rxjs-tests.component';

@NgModule({
  declarations: [
    AppComponent,
    PausableDemoComponent,
    PausableFilterDemoComponent,
    PausableFilterWithFilterDemoComponent,
    RxjsTestsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
