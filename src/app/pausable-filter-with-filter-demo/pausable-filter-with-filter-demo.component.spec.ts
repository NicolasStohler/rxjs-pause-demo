import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PausableFilterWithFilterDemoComponent } from './pausable-filter-with-filter-demo.component';

describe('PausableFilterWithFilterDemoComponent', () => {
  let component: PausableFilterWithFilterDemoComponent;
  let fixture: ComponentFixture<PausableFilterWithFilterDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PausableFilterWithFilterDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PausableFilterWithFilterDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
