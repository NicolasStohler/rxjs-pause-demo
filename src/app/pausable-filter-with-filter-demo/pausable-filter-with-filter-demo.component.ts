import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, timer } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-pausable-filter-with-filter-demo',
  templateUrl: './pausable-filter-with-filter-demo.component.html',
  styleUrls: ['./pausable-filter-with-filter-demo.component.css']
})
export class PausableFilterWithFilterDemoComponent implements OnInit {

  pauser$ = new BehaviorSubject<boolean>(true);
  evenFilter$ = new BehaviorSubject<boolean>(false);
  timedProducer$: Observable<string>;
  itemLoader$: Observable<string>;

  constructor() { }

  ngOnInit() {
    // tick every second
    const timer$ = timer(0, 1000);

    // produce items from timer ticks
    this.timedProducer$ = timer$.pipe(
      // filter((t) => this.evenFilter$.getValue() ? t % 2 === 0 : true),
      map(t => `item #${t}`),
      // tap(t => console.log('produce item', t))
    );

    // pause using a filter (pauser$), with additional filtering using evenFilter$.
    // It's also possible to move the filters into timedProducer$.pipe above, depends
    // where you fetch and process the data (keep fetching in the background if paused
    // or not).
    this.itemLoader$ = this.timedProducer$.pipe(
      // when paused: just skip values:
      filter(() => !this.pauser$.getValue()),
      // additional filter:
      filter((t) => this.evenFilter$.getValue() ? +t.substring(6) % 2 === 0 : true),
      tap(t => console.log('display item', t, t.substring(6)))
    );
  }

  togglePause() {
    const switchToPaused = !this.pauser$.getValue();
    if (switchToPaused) {
      console.log('pausing now');
    } else {
      console.log('resume now');
    }

    this.pauser$.next(switchToPaused);
  }

  toggleEvenFilter() {
    this.evenFilter$.next(!this.evenFilter$.getValue());
  }
}
